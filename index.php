<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BUSCADRO CON AJAX Y JQUERY</title>
    <script type="text/javascript" src="js/jquery.js"></script>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="preconnect" href="https://fonts.googleapis.com"> <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
</head>
<body>
    <div class="container">
        <div class="center">
           <br><br><br>
            <img src="img/logo.png" width="400">
            <br><br><br><br><br><br>
        </div>                
        <div class="form center">
            <form action="" method="post" name="search_form" id="search_form">
                <input type="text" name="search" id="search">
            </form>
        </div>
        <div id="resultados"></div>
        <div class="footer center">
            Copyright 2023 - todos los derechos reservados <br>
            <a href="">www.programacion_WEB.com</a>
        </div>
    </div>
</body>
</html>